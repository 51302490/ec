  <div class="row">
    <div class="col-md-5  toppad  pull-right col-md-offset-3 ">
      <p class=" text-info">From: <?php echo $members[0]['createday']?></p>
    </div>
    <div class="col-xs-12 col-sm-12 col-sm-10 col-md-8 col-lg-6 col-sm-offset-1 col-md-offset-2 col-lg-offset-3 toppad" >
      <div class="panel panel-info">
        <div class="panel-heading">
          <h3 class="panel-title" id="panel-user-name"><?php echo $members[0]['username'] ?></h3>
        </div>
        <div class="panel-body">
            <div class="col-md-3 col-lg-3" id="equal-height-pic" align="center"> 

              <img alt="User Pic" src="<?php echo base_url().'public/images/'.$members[0]['avatar']?>" class="img-circle img-responsive" id="profile-pic" alt="<?php echo $members[0]['username']?>" title="<?php echo $members[0]['username']?>">
               <div id="change-pic" style="visibility:hidden">
                  <label for="userfile"><i class="fa fa-camera" aria-hidden="true"></i></label>
              </div>
              <div align="left"  style="visibility:hidden">
              <form action="<?php echo base_url().'upload/do_upload/'.$members[0]['ID']?>" method="post" id="form1" enctype="multipart/form-data">
                <input type='file' id="userfile" name="userfile" accept="image/*" onchange="readURL(this);"/>
              </form>
              </div>
            </div>
          <form method="post">


            <div class=" col-md-9 col-lg-9 col-xs-12"> 
    					<div class="form-group">
    						<button id="button" type="button"><span class="glyphicon glyphicon-edit"></span> &nbsp; Edit </button>
    					</div>
    					<div class="form-group">
      					<label for="username">Username</label>
      					<input type="text" class="form-control" value="<?php echo $members[0]['username'] ?>" readonly id="username" name ="username">
    					</div>
              <div class="form-group">
      					<label for="email">Email address</label>
      					<input type="email" class="form-control" value="<?php echo $members[0]['email'] ?>" readonly id="email" name="email">
    					</div>
              <div class="form-group">
                <label for="email">Account Balance</label>
                <input type="number" class="form-control" value="<?php echo $members[0]['money'] ?>" readonly id="money" name="money">
              </div>
   	 				  <div class="form-group nodisplay">
      					<label for="password">Password</label>
      					<input type="password" class="form-control" id="password" name="password">
    					</div>
              <div class="form-group nodisplay">
                <label for="repassword">New Password</label>
                <input type="password" class="form-control" id="repassword" name="repassword">
              </div>
    					<div class="form-group">
    						<label for="birthday">Birthday </label>
    						<input type="date" class="form-control" value="<?php echo $members[0]['birthday'] ?>" id="birthday" readonly name="birthday">
    					</div>
    					<div class="form-group" id="form-save">
                <button type="button" class="btn btn-primary btn-auction nodisplay" id="save" onclick="UpdateInfo(this.form)">Save</button>
    					</div>
            </div>
          </form>
        </div>
        
      </div>
    </div>
  </div>

<script type="text/javascript">
  document.getElementById('button').onclick = function() {
    document.getElementById('email').readOnly = false;
    document.getElementById('username').readOnly = false;
    document.getElementById('birthday').readOnly = false;
    document.getElementById('change-pic').style.visibility='visible';
    document.getElementById('save').style.display = "block";
    $('.nodisplay').fadeIn();
 };

  function UpdateInfo(form){ 
    var url = $("#userfile").val();
    $.ajax('../update-info', {
      type: "POST",
      data:{
        "username" : form.username.value,
        "password" : form.password.value,
        "repassword" : form.repassword.value,
        "birthday" : form.birthday.value,
        "image" : url
      },
      success: function(callback){
        console.log(callback);
        if (callback > 0){
          console.log("succes");
          $('#username').val(form.username.value);
          $('#birthday').val(form.birthday.value);
          $('#username').prop("readonly", true);
          $('#birthday').prop("readonly", true);
          $('#panel-user-name').html(form.username.value);
          $('.nodisplay').fadeOut();
          $('#change-pic').hide();
          $('#header-username-display').html(form.username.value);
          document.getElementById("form1").submit();
        }
        else {
          alert('Wrong Password!')
        }
      },
    });     
  };

  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#profile-pic')
          .attr('src', e.target.result)
        };
      reader.readAsDataURL(input.files[0]);
    }
  }
  $("#pic").change(function(){
    readURL(this);
  });
  var he = $("#profile-pic").width();
  $("#profile-pic").css("height", he);
  $("#change-pic").css("height", he);
  $("#change-pic").css("width", he);
  $("#equal-height-pic").css("height", he);
</script>

