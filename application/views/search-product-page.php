<div class="mybody">
	<table class="table table-hover table-responsive" id="mytable">
		<tr>
			<th>ID</th>
			<th>Hình ảnh</th>
			<th>Tên sản phẩm</th>
		

		</tr>
		<?php if ($product_count==0) {?>
		<h1>No Data Found</h1>
		<?php } ?>
		<?php for ($i = 0; $i < $product_count; $i++){ 
				$product = $product_list[$i];
			?>
			<tr>
				<td><?php echo $product['ID'] ?></td>
				<td><a href="<?php echo base_url().'product/'. $product['ID']?>"><img src="<?php echo base_url().'public/images/'.$product['avatar'] ?>" class="table-pic"> </a></td>
				<td> <a href="<?php echo base_url().'product/'. $product['ID']?>"><?php echo $product['name'] ?></a></td>
			</tr>
		<?php } ?>
	</table>

	<ul class="pagination pagination-md mypage">
		<li class="disabled"><a href="#">Prev</a></li>
		<li class="active"><a href="#">1</a></li>
		<li><a href="#">2</a></li>
		<li><a href="#">3</a></li>
		<li><a href="#">Next</a></li>
	</ul>	
</div>
