<script type="text/javascript">
	function NewAuction_<?php echo $product[0]['ID'] ?>(form){
		var bid = form.new_auction.value;

		<?php if ($this->session->userdata('userID') == false) { ?>
			alert('PLEASE LOGIN FIRST!');
		<?php } else {?>
			$.ajax("<?php echo base_url().'new-auction/'.$product[0]['ID'] ?>",{
				type: "GET",
				data: {
					'bid' : bid
				},
				success: function(callback){
					if (callback != 0){			
						alert('Đấu giá thành công');
						location.reload();					
					}
					else {
						alert('YOUR BID CANNOT BE EQUAL OR LOWER THAN CURRENT BID');
					}
				}
			})
		<?php } ?>
	};
</script>

<div class="mybody">
	<?php include 'snippet/product-slider.php' ?>

<?php $endtime = explode(' ', $product[0]['endtime']);
	$time = $endtime[0].'T'.$endtime[1];
?>
	<div class="product-description col-sm-6">
		<h2 class="product-name">
			<strong><?php echo $product[0]['name'] ?></strong>
		</h2>
		<p class="product-detail">
			<strong>ID:</strong></h3>
			<?php echo $product[0]['ID'] ?> 
		</p>
		<p class="product-detail">
			<strong>Tình trạng:</strong> 
			<?php echo $product[0]['product_condition'] ?> 
		</p>
		<p class="product-detail">
			<strong>Người bán:</strong> 
			<?php echo $product[0]['username'] ?> 
		</p>
		<p class="product-detail">
			<strong>Giá khởi điểm:</strong>
			<?php echo $product[0]['startprice'] ?>
		</p>
		<p class="product-detail">
			<strong>Số lượt đấu giá:</strong>
			<?php echo $product[0]['auctions'] ?>
		</p>
		<p class="product-detail">
			<?php  
			$originalDate = $product[0]['starttime']; 
			$newDate = date("d/m/Y H:i:s",strtotime($originalDate));
			echo $newDate;
			?>
		</p>
		<p class="product-detail">
			<?php  
			$originalDate = $product[0]['endtime']; 
			$newDate = date("d/m/Y H:i:s",strtotime($originalDate));
			echo $newDate;
			?>
		</p>
		<p class="product-time">
			<img src="<?php echo base_url()?>public/images/time-left.png">
			<iframe src="http://free.timeanddate.com/countdown/i5gqqvp0/n218/cf12/cm0/cu4/ct0/cs1/ca0/co0/cr0/ss0/cac23527c/cpc23527c/pcfff/tcfff/fn3/fs100/szw448/szh189/iso<?php echo $time ?>" allowTransparency="true" frameborder="0" width="150" height="37"></iframe>
		</p>
		<p class="product-detail product-inline">
			<strong>Giá hiện tại:</strong>
		</p>
		<p class="product-price product-inline">
			<?php echo $product[0]['currentbid'] ?>
		</p>
		<p>
			<form>
				<input type="number" name="new_auction" placeholder="Đặt giá mới" class="product-new-price">
				<button class="btn btn-primary btn-auction" type="button" onclick="NewAuction_<?php echo $product[0]['ID'] ?>(this.form)">Đấu giá</button>
			</form>
		</p>
	</div>

	<?php $file = 'snippet/'.$product[0]['description'] ?>
	<?php include $file ?>
	<?php include 'snippet/product-cmt.php' ?>
</div>
