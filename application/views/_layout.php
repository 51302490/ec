<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<title><?php echo $title ?></title>
	<meta name="keywords" content="<?php echo $keywords ?>">
	<meta name="description" content="<?php echo $description ?>">
	<link rel="shorcut icon"  href="Data/Images/hammer-pink.png">
	
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	<script src="https://use.fontawesome.com/67a826b370.js"></script>
	
	<!-- INCLUDE PAGE CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/index.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/about-us-page.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/auction-result-page.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/product-description-page.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/auction-history-page.css">

	<!-- INCLUDE SNIPPET CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/fonts.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/member.css">
	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/auction-end.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/headerfooter.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/profile.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/product-info.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/product-cmt.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/add-product.css">	
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/auction.css">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url()?>public/css/search.css">

	<!-- INCLUDE JS -->
	
</head>
<body>
		
	<?php include 'snippet/login.php' ?>
	<?php include 'snippet/signup.php' ?>
	<?php include 'snippet/contact.php' ?>

	<?php include'snippet/header.php' ?>
	<?php include 'snippet/gotop.php' ?>

	<?php $this->load->view($contentPage); ?>

	<?php include 'snippet/footer.php' ?>
	
</body>
</html>