<div class="mybody">
	<?php if ($product_count == 0) { ?>
		<p class="return-no-product">Không tìm thấy sản phẩm</p>
	<?php } else { ?>
		<div class="clearfix hidden-sm hidden-xs">
			<?php 
			if($product_count % 4 == 0)
				$rows = $product_count/4;
			else
				$rows = $product_count/4 + 1;
			
			for ($i = 0; $i < $rows ; $i++) { 
				$col = 0;
			?>
				<div class="clearfix">
				<?php while ($col < 4 && ($i*4 + $col < $product_count)) {
					$product = $product_list[$i*4 + $col];
					$col++;
					include 'snippet/auction.php' ?>
				<?php } ?>
				</div>			
			<?php } ?>
		</div>

		<div class="clearfix hidden-lg hidden-md">
			<?php 
				if($product_count % 3 == 0)
					$rows = $product_count/3;
				else
					$rows = $product_count/3 + 1;

				for ($i = 0; $i < $rows; $i++) { 
					$col = 0;
				?>
					<div class="clearfix">
					<?php while($col < 3 && ($i*3 + $col < $product_count)) {
						$product = $product_list[$i*3 + $col]; 
						$col++;
						include 'snippet/auction.php' ?>
					<?php } ?>
					</div>			
			<?php } ?>
		</div>
		
		<ul class="pagination pagination-md mypage">
			<li class="disabled"><a href="#">Prev</a></li>
			<li class="active"><a href="#">1</a></li>
			<li><a href="#">2</a></li>
			<li><a href="#">3</a></li>
			<li><a href="#">Next</a></li>
		</ul>	
	<?php } ?>
</div>

<script type="text/javascript">	
	$('.btn-auction').mouseover(function(){
		$(this).css("transition", "width 1s");
		$(this).css("width","90%");
	});
	$('.btn-auction').mouseout(function(){
		$(this).css("transition", "width 1s");
		$(this).css("width","100px");
		$(this).css("outline","none");
	});
</script>