<div class="mybody">
	<?php include 'snippet/product-slider.php' ?>

	<div class="product-description col-sm-6">
		<p class="product-name">
			<?php echo $product[0]['name'] ?>
		</p>
		<p class="product-detail">
			<strong>ID:</strong> 
			<?php echo $product[0]['ID'] ?>
		</p>
		<p class="product-detail">
			<strong>Tình trạng:</strong> 
			<?php echo $product[0]['product_condition'] ?>
		</p>
		<p class="product-detail">
			<strong>Người bán:</strong>
			<?php echo $product[0]['seller'] ?>
		</p>
		<p class="product-detail">
			<strong>Giá khởi điểm:</strong>
			<?php echo $product[0]['startprice'] ?>
		</p>
		<p class="product-detail">
			<strong>Số lượt đấu giá:</strong>
			<?php echo $product[0]['auctions'] ?>
		</p>
		<p class="product-detail">
			<strong>Kết thúc:</strong> 
			<?php echo $product[0]['endtime'] ?>
		</p>
		<p class="product-detail product-inline">
			<strong>Giá kết thúc:</strong>
		</p>
		<p class="product-price product-inline">
			<?php echo $product[0]['currentbid'] ?>
		</p>
		<p class="product-detail">
			<strong>Người thắng:</strong>
			<span class="product-end-winner"> 
				<?php echo $product[0]['winner'] ?>
			</span>
		</p>
	</div>

	<?php $file = 'snippet/'.$product[0]['description'] ?>
	<?php include $file ?>
	<?php include 'snippet/product-cmt.php' ?>
</div>
