<!-- Slide 1 -->
<div class="my-vertical-slide" id="slide1">
    <div class="about-para hidden-xs">
        <div class="about-para-content">
            <h3>Thỏa sức mua sắm. An tâm chất lượng</h3>
        </div>
    </div>
</div>

<!-- Slide 2 -->
<div class="my-vertical-slide" id="slide2">
    <div class="about-us-contain clearfix">
        <h2 class="about-title"><strong>About Us</strong></h2>
        <div class="about-us-info col-sm-7">
            <p class="about-us-para" title="Giới thiệu trang web">Auction Online chuyên cung cấp những sản phẩm chất lượng, thông tin chân thực nhất đến với người dùng. "Đơn giản, thân thiện, trung thực" luôn là tôn chỉ của website chúng tôi. Trải qua gần một thập kỉ xây dựng và phát triển, Auction Online đã chứng minh được uy tín của mình, trở thành website yêu thích nhất trong cả nước của những người yêu thích nghệ thuật đấu giá.</p>
            <p class="about-us-para">Bạn đã sẵn sàng cùng chúng tôi khám phá vùng đất tuyệt vời này chưa?</p>
            <button class="btn btn-success btn-joinus" data-toggle="modal" data-target="#SignInModal">Join Us</button>
        </div>  
        <div class="about-us-info col-sm-5">
            <img src="<?php echo base_url()?>public/images/about-us02.jpg" class="fullsize-pic hidden-xs" alt="auction easy" title="auction easy">
            <img src="<?php echo base_url()?>public/images/about-us01.jpg" class="halfsize-pic hidden-xs" alt="bid picture" title="bid picture">
            <img src="<?php echo base_url()?>public/images/about-us03.jpg" class="halfsize-pic hidden-xs" alt="auction promotion" title="auction promotion">
        </div>              
    </div>
</div>

<!-- Slide 3 -->
<div class="my-vertical-slide" id="slide3">
    <div class="contact-us animated fadeInUp ">
        <h2>Let's talk! We're listening</h2>
        <form>
            <div class="clearfix">
                 <div class="contact-us-info col-sm-6">
                    <table>
                        <tr>
                            <td><label for="contact-name">Your name*</label></td>
                            <td><input type="name" name="" id="contact-name" required="required"></td>
                        </tr>
                        <tr>
                            <td><label for="contact-email">Email*</label></td>
                            <td><input type="email" name="" id="contact-email"></td>
                        </tr>
                        <tr>
                            <td><label for="contact-phone">Phone</label></td>
                            <td><input type="number" name="" id="contact-phone"></td>
                        </tr>
                        <tr>
                            <td><label for="contact-title">Title*</label></td>
                            <td><input type="text" name="" id="contact-title" required="required"></td>
                        </tr>
                    </table>
                </div>
                <div class="contact-us-info col-sm-6">
                    <table>
                        <tr>
                            <td><label for="contact-content">Content*</label></td>
                            <td><textarea id="contact-content" required="required"></textarea></td>
                        </tr>
                    </table>
                </div>
            </div>
            <button class="btn btn-success btn-submit">Submit</button>
        </form>
    </div>
</div>

<!-- Slide 4 -->
<?php 
$members = array(
    array('Ngân Kim', 'Founder', 'Time doesnt make history. People do!', 'member-ngan.jpg'),
    array('Trọng Trí', 'Co-founder', 'Dont limit your challenges. Challenge your limits.', 'member-danh.jpg'),
    array('Viết Huy Phan', 'Manager', 'Every damn day. Just do it!', 'member-hao.jpg')
    );
?>
<div class="my-vertical-slide clearfix" id="slide4">
    <div class="members clearfix animated fadeInUp">
        <h2 class="member-title"><strong>Members</strong></h2>
        <?php for ($i = 0; $i < 3; $i++){
            $member = $members[$i];
            include 'snippet/members.php';
         } ?>
    </div>
</div>

       


