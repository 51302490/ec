 <div class="modal fade" id="ContactModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel2">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ModalLabel2">Contact Us</h4>
      </div>
      <div class="modal-body">
        <form class="omb_loginForm" action="" autocomplete="off" method="POST">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
            <input type="text" class="form-control" placeholder="Nguyễn Văn A" required="required">
          </div>
          <span class="help-block"></span>
          <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
              <input  type="number" class="form-control" placeholder="01245 122 122" required="required">
          </div>
          <span class="help-block"></span>
           <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
              <input  type="email" class="form-control"  placeholder="example@gmail.com" required="required">
          </div>
          <span class="help-block"></span>
          <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-comments" aria-hidden="true"></i></span>
              <textarea type="text" class="form-control"></textarea> 
          </div>
          <div class="modal-footer">
            <button class="btn btn-lg btn-primary btn-block" type="submit">
            Submit</button>
          </div>
        </form>
      </div>  
    </div>
  </div>
</div>