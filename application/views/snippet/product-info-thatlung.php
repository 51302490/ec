      
<div class="product-info col-xs-12">
      <h3 class="product-info-title">Giới thiệu về sản phẩm</h3>
      
      <h3 class="product-info-feature"> Chất liệu da thật chất lượng cao</h3>
      <p class="product-info-deatail">thắt lưng sử dụng chất liệu từ da bò cao cấp, bề mặt bền đẹp, được xử lý và gia công theo tiêu chuẩn cao. Khi dùng lực kéo, thắt lưng da bò rất chặt và không có độ giãn, mặt sau được chia theo sớ tự nhiên, không đều tăm tắp như silicon. Quan trọng nhất, khi đốt trên ngọn lửa, dây bằng silicon sẽ để lại vết cháy và vón cục, với thắt lưng da bò thật sẽ không có dấu vết để lại. Bạn có thể yên tâm sử dụng tối đa tiện ích của sản phẩm mà không lo dây nhanh hỏng hoặc rách.</p>

      <h3 class="product-info-feature">Thiết kế tinh tế và thanh lịch</h3>

      <p class="product-info-deatail">Thiết kế 1 lớp, phần đầu gài làm bằng hợp kim sáng bóng, họa tiết giả da cá sấu thời thượng, kiểu dáng đẹp mắt mang đến cho bạn vẻ sành điệu và phong cách.</p>

      <p><img title="Thắt lưng" alt="Thắt lưng" src="<?php echo base_url()?>public/images/thatlung2.jpg" class="product-info-pic img-responsive"></p>

      <h3 class="product-info-feature">Màu sắc sang trọng</h3>

      <p class="product-info-deatail">Sở hữu màu sắc đơn giản nhưng sang trọng, chiếc thắt lưng dễ dàng được phối với các loại trang phục khác nhau, tôn lên nét cá tính của bạn.</p>
      
      <p><img title="Thắt lưng" alt="Thắt lưng" src="<?php echo base_url()?>public/images/thatlung3.jpg" class="product-info-pic img-responsive"></p>

      <h3 class="product-info-feature" > Đặc điểm chính</h3>

      <p class="product-info-deatail">
      Chất liệu vải:Da thuộc <br>
      Màu : Đen <br>
      Kích thước sản phẩm (D x R x C cm): 17x13x4 <br>
      Trọng lượng (KG):  0.3 <br>
      Loại hình bảo hành  :  Không hỗ trợ bảo hành <br>
      </p>
</div>
