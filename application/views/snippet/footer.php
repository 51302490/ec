<footer>
<div id='bttop' class="hidden-xs"><img id="img-top" src="/Data/Images/top.png"></div>
<div class="keyword hidden-xs">
  <span>Tìm Kiếm Nhiều</span>
  <a href="#">Iphone 7</a>
  <a href="#">Galaxy</a>
  <a href="#">HTC</a>
  <a href="#">Điện Thoại</a>
  <a href="#">Nồi Cơm Điện</a>
</div>
<div class="footer-z">
  <div class="container">
    <div class="col-sm-4 col-xs-12">
      <h2 class="footer-title">Hỗ Trợ Khách Hàng</h2>
      <ul class="footer-support">
        <li><a href="#">Giao Hàng & Thanh Toán </a></li>
        <li><a href="#">Hướng Dẫn Đấu Giá Online</a></li>
        <li><a href="#">Chính Sách Đổi Trả</a></li>
        <li><a href="#">Gửi Góp Ý Khiếu Nại</a></li>
        <li><a href="#">Tuyển Dụng</a></li>
      </ul>
    </div>
    <div class="col-sm-4 hidden-xs">
      <h2 class="footer-title">Contact Web</h2>
      <ul class="footer-logo">
          <li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></i></a></li> 
          <li><a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
          <li><a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a></li>
          <li><a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a></li>
      </ul>
    </div>
    <div class="col-sm-4 col-xs-12">
      <h2 class="footer-title">Contact Us</h2>
      <p class="footer-contact-info"><i class="fa fa-home" aria-hidden="true"></i> 23/58 Lý Thường Kiệt Quận 10, TP.HCM </p>
      <p class="footer-contact-info"><i class="fa fa-phone" aria-hidden="true"></i> 01264895603 </p>
      <p class="footer-contact-info"><i class="fa fa-envelope" aria-hidden="true"></i> contact@auctiononline.com </p>
    </div>
  </div>
</div>
<div class="footer-bottom hidden-xs">
      <div class="keyword">
        <span>@Ecommerce Team</span>
      </div>
</div>
</footer>