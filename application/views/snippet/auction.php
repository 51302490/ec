<?php $endtime = explode(' ', $product['endtime']);

	$time = $endtime[0].'T'.$endtime[1];
	$pID = $product['ID'];
	?>
<script type="text/javascript">
	function NewAuction_<?php echo $pID ?>(form){
		var bid = form.new_auction.value;

		<?php if ($this->session->userdata('userID') == false) { ?>
			alert('PLEASE LOGIN FIRST!');
		<?php } else {?>
			$.ajax("<?php echo base_url().'new-auction/'.$product['ID'] ?>",{
				type: "GET",
				data: {
					'bid' : bid
				},
				success: function(callback){
					if (callback != 0){	
						alert('Đấu giá thành công');		
						location.reload();
					}
					else {
						alert('YOUR BID CANNOT BE EQUAL OR LOWER THAN CURRENT BID');
					}
				}
			})
		<?php } ?>
	};
</script>

<div class="auction-product col-md-3 col-sm-4">
	<form>
		<a href="<?php echo base_url().'product/'. $product['ID']?> "> <img src="<?php echo base_url(). 'public/images/'.$product['avatar'] ?>" alt="<?php echo $product['name'] ?>" title="<?php echo $product['name'] ?>"> </a>
		<a href="<?php echo base_url().'product/'. $product['ID']?> " class="nonedeco"><h3 class="product-name"><strong><?php echo $product['name']?></strong></h3></a>
		<p class="product-time">
			<iframe src="http://free.timeanddate.com/countdown/i5gqkxua/n218/cf12/cm0/cu4/ct0/cs1/ca0/co0/cr0/ss0/cac23527c/cpc23527c/pcfff/tcfff/fn3/fs100/szw448/szh189/iso<?php echo $time ?>" allowTransparency="true" frameborder="0" width="150" height="37"></iframe>
		</p>
		<p class="currentbid">Giá hiện tại: <span> <?php echo $product['currentbid']?></span></p>
		<input type="number" class="inp-bid" name="new_auction">
		<br>
		<button class="btn btn-primary btn-auction" type="button" onclick="NewAuction_<?php echo $pID ?>(this.form)">Đấu giá</button>
	</form>
</div>


