<script type="text/javascript">
  var UserSignUp = function(form) {
    $.ajax('<?php echo base_url() ?>signup', {
      type: 'POST',
      data: {
        'username' : form.username.value,
        'password' : form.password.value,
        'repassword' : form.repassword.value,
        'birthday' : form.dob.value,
        'email' : form.email.value
      },
      success: function(){
         window.location.href = '<?php echo base_url()?>home';
      }
    })
  }
</script>

 <div class="modal fade" id="SignInModal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel2">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ModalLabel2">Sign Up</h4>
      </div>
      <div class="modal-body">
        <form class="omb_loginForm" action="" autocomplete="off" method="POST">
          <div class="input-group">
            <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <input type="text" class="form-control" name="username" placeholder="username" required="required">
          </div>
          <span class="help-block"></span>
        
          <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>
              <input  type="password" class="form-control" name="password" placeholder="Password" required="required">
          </div>
          <span class="help-block"></span>
           <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-lock"></i></span>
              <input  type="password" class="form-control" name="repassword" placeholder="Re-Password" required="required">
          </div>
          <span class="help-block"></span>
          <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-birthday-cake" aria-hidden="true"></i></span>
              <input  type="date" class="form-control" name="dob">
          </div>
            <span class="help-block"></span>       

           <div class="input-group">
              <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
              <input  type="email" class="form-control" name="email" placeholder="email" required="required">
          </div>  
          <span class="help-block"></span>
            <label><input type="checkbox" name="terms" required="required"> I agree with the <a href="#">Terms and Conditions</a>.</label>
          <div class="modal-footer">
            <button class="btn btn-lg btn-primary btn-block" type="button" onclick="UserSignUp(this.form)">
            Sign up</button>
          </div>                    
        </form>
      </div>
      
    </div>
  </div>
</div>