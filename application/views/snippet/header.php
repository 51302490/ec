 <script type="text/javascript"> 
        function ajaxSearch() {
            var input_data = $('#search_data').val();
            if (input_data.length === 0) {
                $('#suggestions').hide();
            } else {

                var post_data = {
                    'search_data': input_data,
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                };

                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>autosearch",
                    data: post_data,
                    success: function(data) {
                        // return success
                        if (data.length > 0) {
                            $('#suggestions').show();
                            $('#autoSuggestionsList').addClass('auto_list');
                            $('#autoSuggestionsList').html(data);
                        }
                    }
                });

            }
        }

</script>
<header id="myheader">
<section class="head">  
<nav class="navbar navbar-default " role="navigation" >
  <div class="container">
      <div class="mar-top" class="col-md-12 col-sm-12">
        <div class="row">
          <div class="col-sm-5 col-xs-12"> 
            <h1 class="title"> Auc<img src="<?php echo base_url()?>public/images/hammer-pink.png" style="width: 40px;" alt="hammer-icon" title="hammer-icon">ion Online</h1> 
          </div>
          <div class="header-logo col-sm-4 hidden-xs">
              <a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></i></a>
              <a href=""><i class="fa fa-twitter" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-google-plus" aria-hidden="true"></i></a>
              <a href=""><i class="fa fa-youtube-play" aria-hidden="true"></i></a>
          </div>
          <div class="header-btn col-sm-3 col-12-xs" class="log-button">
            <?php if ($this->session->userdata('userName') == FALSE) { ?>
              <button data-toggle="modal" data-target="#loginmodal" id="btn-login">LOGIN</button>
              
              <button class="btn btn-primary" data-toggle="modal" data-target="#SignInModal" data-whatever="@signin" class="log" id="btn-signup">SIGN UP</button>
            
            <?php } else { ?>
              <ul class="header-dropdown-ava">
                <li class="dropdown">
                  <a href="#">
                    <img src="<?php echo base_url().'public/images/'.$this->session->userdata('userAvatar') ?>" class="img-circle header-ava">
                  </a>
                  <div class="dropdown-content">
                    <a href="<?php echo base_url().'member-info/'.$this->session->userdata('userID')?>">Thông tin cá nhân</a>
                    <a href="<?php echo base_url()?>auction-history">Lịch sử đấu giá</a>
                    <a href="<?php echo base_url()?>add-product">Thêm sản phẩm</a>
                    <a href="<?php echo base_url() ?>logout" id="logout-click"><i class="fa fa-power-off" aria-hidden="true"></i> &nbsp; &nbsp;Logout</a>
                  </div>
                </li>
                <li>
                  <a href="#" class="header-logout" id="header-username-display"> <?php echo $this->session->userdata('userName') ?> </a>
                </li>
              </ul>
            <?php } ?>
          </div>
      </div>
    </div>
  </div> 
  <div class="header-border clearfix">
    <div class="container" id="menumega">
      <nav class="navbar navbar-default" id="nomargin">
        <div class="navbar-header">

          <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".js-navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
        </div>

        <div class="collapse navbar-collapse js-navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="dropdown mega-dropdown">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown" id="header-sanpham">SẢN PHẨM &nbsp; &nbsp; <span class="glyphicon glyphicon-chevron-down pull-right"></span></a>
              <ul class="dropdown-menu mega-dropdown-menu row">
                <li class="col-sm-3 col-md-3">
                  <ul>
                    <li class="dropdown-header">New</li>
                    <div id="myCarousel" class="carousel slide" data-ride="carousel">
                      <div class="carousel-inner">
                        <div class="item active">
                          <a href="#"><img src="<?php echo base_url()?>public/images/hinhslide3.jpg" class="img-responsive" alt="OPPO Neo 3" title="OPPO Neo 3"></a>
                          <h4><small>Summer</small></h4>
                          <button class="btn btn-primary" type="button">2.000.000 </button>
                          <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Đấu giá ngay</button>
                        </div>
                        <!-- End Item -->
                        <div class="item">
                          <a href="#"><img src="<?php echo base_url()?>public/images/hinhslide1.jpg" class="img-responsive" alt="LG Optimus Black" title="LG Optimus Black"></a>
                          <h4><small>Autum</small></h4>
                          <button class="btn btn-primary" type="button">5.000.000 </button>
                          <button href="#" class="btn btn-default" type="button"><span class="glyphicon glyphicon-heart"></span> Đấu giá ngay</button>
                        </div>
                        <!-- End Item -->
                      </div>
                      <!-- End Carousel Inner -->
                    </div>
                    <!-- /.carousel -->
                  </ul>
                </li>
                <li class="col-sm-3 col-md-3">
                  <ul>
                    <li class="dropdown-header">
                    <a href="home">Đang Đấu Giá</a> </li>
                    <li><a href="<?php echo base_url()?>home/technology">Công Nghệ</a></li>
                    <li><a href="<?php echo base_url()?>home/household">Gia Dụng</a></li>
                    <li><a href="<?php echo base_url()?>home/fashion">Thời Trang</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url()?>home/others">Khác</a></li>
                  </ul>
                </li>
                <li class="col-sm-3 col-md-3">
                  <ul>
                    <li class="dropdown-header">
                      <a href="<?php echo base_url()?>recently-end">Mới Kết Thúc</a></li>
                    <li><a href="<?php echo base_url()?>recently-end/technology">Công Nghệ</a></li>
                    <li><a href="<?php echo base_url()?>recently-end/household">Gia Dụng</a></li>
                    <li><a href="<?php echo base_url()?>recently-end/fashion">Thời Trang</a></li>
                    <li class="divider"></li>
                    <li><a href="<?php echo base_url()?>recently-end/others">Khác</a></li>
                  </ul>
                </li>
                <li class="col-sm-3 col-md-3">
                  <ul>
                    <li class="dropdown-header"> <a href="<?php echo base_url()?>auction-result">Kết Quả</a></li>
                    <li class="divider"></li>
                    <form class="form" role="form" action="<?php echo base_url()?>search" method="post"> 
                      <div class="form-group">
                        <label class="sr-only" for="key">Search</label>
                        <input type="text" class="form-control" id="key" required="" placeholder="search..." name="key">
                      </div>
                      <button type="submit" class="btn btn-primary btn-block" >Search</button>
                    </form>
                  </ul>
                </li>
              </ul>
            </li>
          </ul>

          <ul class="nav navbar-nav navbar-left header-menu">
            <li><a href="<?php echo base_url()?>home">HOME</a></li>
            <li><a href="<?php echo base_url()?>about-us">ABOUT US</a></li>
            <li><a href="#" data-toggle="modal" data-target="#ContactModal">CONTACT US</a></li>
          </ul>
          <form class="form" role="form" action="<?php echo base_url()?>search" method="post">
            <div class="input-group search-group hidden-xs">       
              <input type="text" class="form-control" name="search_data"  placeholder="Search..." required="" id="search_data" onkeyup="ajaxSearch();">
            <div id="suggestions">
            <div id="autoSuggestionsList">  
            </div>
            </div>
              <span class="input-group-btn">
                  <button class="btn btn-default" type="submit"><img src="<?php echo base_url()?>public/images/search.png" class="search-button"></button>
              </span>
            
            </div>
          </form>
        </div>
        <!-- /.nav-collapse -->
               
      </nav>
    </div>
  </div> 
 
  </div>
</nav>
  

</section>
</header>
<script type="text/javascript">
  jQuery(document).on('click', '.mega-dropdown', function(e) {
  e.stopPropagation()
});
  $(function(){
        // Check the initial Poistion of the Sticky Header
        var stickyHeaderTop = $('.header-border').offset().top;
 
        $(window).scroll(function(){
                if( $(window).scrollTop() > stickyHeaderTop ) {
                  $('.header-border').css({position: 'fixed', top: '0px'});
                } else {
                  $('.header-border').css({position: 'static', top: '0px'});
                }
        });
  });

</script>