
<div class="product-info col-xs-12">
      <h3 class="product-info-title">Giới thiệu về sản phẩm</h3>
      
      <h3 class="product-info-feature">Chất liệu cao cấp</h3>
      <p class="product-info-deatail">Giày mọi nam POSA M003 được làm từ da kết hợp nhung cao cấp, rất mềm mại và êm ái, tạo cảm giác thoải mái cho từng bước đi. Phần đế làm bằng cao su tổng hợp với phần rãnh chống trơn trượt, đảm bảo sự an toàn cho người mang.
      Kiểu dáng tinh tế, hợp xu hướng.</p>

      <p><img title="Giày lười POSA M003" alt="Giày" src="<?php echo base_url()?>public/images/giayluoinam.jpg" class="product-info-pic img-responsive"></p>

      <h3 class="product-info-feature">Thiết kế trẻ trung</h3>

      <p class="product-info-deatail">Vận dụng sức mạnh từ công nghệ in tiên tiến CAPT và kiến trúc nén thông minh Hi-ScoA, thiết bị này có thể xử lý các dữ liệu hình ảnh nhanh hơn các máy in thông thường bằng cách nén dữ liệu in vào một kích thước nhỏ hơn để có thể nhanh chóng truyền dữ liệu đó từ máy tính sang máy in có CAPT. Tính năng này giúp tăng tốc độ in mà không phải nâng cấp bộ nhớ máy in.</p>

      <p><img title="Giày lười POSA M003" alt="Giày" src="<?php echo base_url()?>public/images/giay1.jpg" class="product-info-pic img-responsive"></p>
      <h3 class="product-info-feature">Dễ phối đồ</h3>

      <p class="product-info-deatail">Giày mọi nam POSA M003 là một “item” đa năng khi có thể dễ dàng phối với bất kì trang phục nào. Chỉ cần một chút nhấn nhá với các phụ kiện, bạn đã có ngay một set đồ hoàn hảo để tự tin đến nơi công sở hoặc dạo phố.</p>
      
    <p><img title="Giày lười POSA M003" alt="Giày" src="<?php echo base_url()?>public/images/giay2.jpg" class="product-info-pic img-responsive"></p>

      <h3 class="product-info-feature">Gợi ý phối đồ</h3>

      <p class="product-info-deatail"> 
      Ở nơi công sở hoặc gặp đối tác bạn hãy thử phối đôi giày này với những bộ vest lịch lãm, những bộ comple sang trọng như:
      + Quần âu – Áo mi
      + Quần tây – Áo sơ mi
      + Quần Kaki - Áo thun/sơ mi body… kèm với các phụ kiện khác như thắt lưng,đồng hồ, caravat… chắc chắn bạn sẽ trông bảnh bao và nam tính phong độ hơn.
      Cách phối đồ sẽ tạo ra sự khác biệt lớn cho phong cách của bạn.Hãy phối theo bất kỳ phong cách nào bạn yêu thích cũng sẽ là một đều mới mẻ đáng để khám phá.</p>

    <p><img title="Giày lười POSA M003" alt="Giày" src="<?php echo base_url()?>public/images/giay3.jpg" class="product-info-pic img-responsive"></p>
</div>
