 <script type="text/javascript">
  $("#close-login").click(function(){
    $("#loginmodal").modal("hide");
  });

  var Userlogin = function(form){
    $.ajax('<?php echo base_url() ?>login', {
      type: 'POST',
      data: {
        'email' : form.email.value,
        'password' : form.password.value
      },
      success: function(data){
        var result = $.trim(data);
        if(result==="wrong")
        {
          alert('PLEASE RETYPE YOUR USERNAME OR PASSWORD');
          $('#login-email').val("Wrong User Name Or Password");

        }
        else{
        window.location.href = '<?php echo base_url()?>home';
        }
      },

    })
  }

</script>

 <div class="modal fade" id="loginmodal" tabindex="-1" role="dialog" aria-labelledby="ModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="ModalLabel">Login or <a href="#" data-toggle="modal" data-target="#SignInModal" id="close-login">Sign up</a></h4>
      </div>
      <div class="modal-body">                
        <div class="container"> 
          <div class="omb_login"> 
            <div class="row omb_row-sm-offset-3 omb_socialButtons">
              <div class="col-xs-4 col-sm-2">
                <a href="#" class="btn btn-lg btn-block omb_btn-facebook">
                  <i class="fa fa-facebook visible-xs"></i>
                  <span class="header-logo-name hidden-xs">Facebook</span>
                </a>
              </div>
              <div class="col-xs-4 col-sm-2">
                <a href="#" class="btn btn-lg btn-block omb_btn-twitter">
                  <i class="fa fa-twitter visible-xs"></i>
                  <span class="header-logo-name hidden-xs">Twitter</span>
                </a>
              </div>  
              <div class="col-xs-4 col-sm-2">
                <a href="#" class="btn btn-lg btn-block omb_btn-google">
                  <i class="fa fa-google-plus visible-xs"></i>
                  <span class="header-logo-name hidden-xs">Google+</span>
                </a>
              </div>  
            </div>

            <div class="row omb_row-sm-offset-3 omb_loginOr">
               <div class="col-xs-12 col-sm-6">
                  <hr class="omb_hrOr">
                  <span class="omb_spanOr">Or</span>
                </div>
            </div>

            <div class="row omb_row-sm-offset-3">
              <div class="col-xs-12 col-sm-6">  
                <form class="omb_loginForm" action="" autocomplete="off" method="POST">                 
                  <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-envelope" aria-hidden="true"></i></span>
                    <input type="text" class="form-control" name="email" id="login-email" placeholder="Email address" required="required">
                  </div>
                  <span class="help-block"></span>
                
                  <div class="input-group">
                      <span class="input-group-addon"><i class="fa fa-lock"></i></span>
                      <input  type="password" class="form-control" name="password" placeholder="Password" id="login-password" required="required">
                  </div>
                  <span class="help-block"></span>
                  <div class="row omb_row-sm-offset-3">
                    <div class="col-xs-12 col-sm-6">
                      <input type="checkbox" value="remember-me">
                      <label> Remember Me </label>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                      <p class="omb_forgotPwd">
                        <a href="#">Forgot password?</a>
                      </p>
                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" name="submit" value="login" onclick="Userlogin(this.form)" class="btn btn-primary modal-button" id="login">Login</button>
                  </div>
                </form>
              </div>
            </div>        
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

