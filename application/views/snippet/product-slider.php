<div class="product-description-left col-sm-6">
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
	    <!-- Indicators -->
	    <ol class="carousel-indicators">
	      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
	      <li data-target="#myCarousel" data-slide-to="1"></li>
	      <li data-target="#myCarousel" data-slide-to="2"></li>
	      <li data-target="#myCarousel" data-slide-to="3"></li>
	    </ol>

	    <!-- Wrapper for slides -->
	    <div class="carousel-inner" role="listbox">
	      <div class="item active">
	        <img src="<?php echo base_url()?>public/images/canon01.jpg" alt="Canon LBP 2900-01" title="Canone LBP 2900-01">
	      </div>

	      <div class="item">
	        <img src="<?php echo base_url()?>public/images/canon02.jpg" alt="Canon LBP 2900-02" title="Canone LBP 2900-02">
	      </div>
	    
	      <div class="item">
	        <img src="<?php echo base_url()?>public/images/canon03.jpg" alt="Canon LBP 2900-03" title="Canone LBP 2900-03">
	      </div>

	      <div class="item">
	        <img src="<?php echo base_url()?>public/images/canon04.jpg" alt="Canon LBP 2900-04" title="Canone LBP 2900-04">
	      </div>
	    </div>
	</div>	
</div>