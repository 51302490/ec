
<div class="product-info col-xs-12">
      <h3 class="product-info-title">Giới thiệu về sản phẩm</h3>
      
      <h3 class="product-info-feature">Tốc độ in cực nhanh</h3>
      <p class="product-info-deatail">Với Canon LBP 2900, bạn sẽ không phải tốn quá nhiều thời gian cho việc in ấn. Bạn sẽ có ngay bản in đầu tiên chỉ sau 9.3 giây mà vẫn đảm bảo chất lượng ổn định. Đối với việc in văn bản, tài liệu, máy có thể đạt tốc độ in 12 trang A4 trong một phút.</p>

      <p><img title="Máy In Laser Canon LBP 2900" alt="Máy In Laser Canon LBP 2900" src="<?php echo base_url()?>public/images/canon01.jpg" class="product-info-pic img-responsive"></p>

      <h3 class="product-info-feature">Công nghệ CAPT 2.1 tiên tiến</h3>

      <p class="product-info-deatail">Vận dụng sức mạnh từ công nghệ in tiên tiến CAPT và kiến trúc nén thông minh Hi-ScoA, thiết bị này có thể xử lý các dữ liệu hình ảnh nhanh hơn các máy in thông thường bằng cách nén dữ liệu in vào một kích thước nhỏ hơn để có thể nhanh chóng truyền dữ liệu đó từ máy tính sang máy in có CAPT. Tính năng này giúp tăng tốc độ in mà không phải nâng cấp bộ nhớ máy in.</p>

      <p><img title="Máy In Laser Canon LBP 2900" alt="Máy In Laser Canon LBP 2900" src="<?php echo base_url()?>public/images/canon02.jpg" class="product-info-pic img-responsive"></p>

      <h3 class="product-info-feature">Tối ưu hóa chất lượng in</h3>

      <p class="product-info-deatail">Với độ phân giải tối đa lên đến 2400 x 600 dpi, Canon LBP 2900  sẽ mang đến cho bạn những bản in đen trắng rõ ràng và sắc nét. Bạn có thể in giấy tờ, văn bản một cách nhanh chóng và dễ dàng nhất.</p>
      
      <p><img title="Máy In Laser Canon LBP 2900" alt="Máy In Laser Canon LBP 2900" src="<?php echo base_url()?>public/images/canon03.jpg" class="product-info-pic img-responsive"></p>

      <h3 class="product-info-feature">Thiết kế đơn giản, nhỏ gọn</h3>

      <p class="product-info-deatail">Máy có kích thước 370 x 251 x 217 mm và khối lượng 5.7 kg, rất phù hợp để sử dụng trong gia đình hoặc văn phòng có diện tích nhỏ và vừa. Bên cạnh đó, thiết kế đơn giản của thiết bị cũng sẽ giúp cho không gian làm việc của bạn trở nên tinh tế hơn.</p>

      <p><img title="Máy In Laser Canon LBP 2900" alt="Máy In Laser Canon LBP 2900" src="<?php echo base_url()?>public/images/canon04.jpg" class="product-info-pic img-responsive"></p> 
</div>
