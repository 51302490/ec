
<div class="product-info col-xs-12">
      <h3 class="product-info-title">Giới thiệu về sản phẩm</h3>
      
      <h3 class="product-info-feature">Kiểu dáng trẻ trung, năng động</h3>
      <p class="product-info-deatail">Áo thun nam dài tay TITISHOP được thiết kế theo kiểu áo raglan với phần cổ tròn và tay dài nên tạo cảm giác khỏe khoắn và trẻ trung khi mặc.. Hãy mặc áo thun nam dài tay TITISHOP để trở thành một anh chàng cá tính, mạnh mẽ và thể thao, khỏe khoắn trong mắt bạn bè và mọi người bạn nhé</p>

      <p><img title="Áo Thun đẹp chất lượng" alt="Áo Thun" src="<?php echo base_url()?>public/images/aothun2.png" class="product-info-pic img-responsive"></p>

      <h3 class="product-info-feature">Chất liệu thun cotton</h3>

      <p class="product-info-deatail">Vận dụng sức mạnh từ công nghệ in tiên tiến CAPT và kiến trúc nén thông minh Hi-ScoA, thiết bị này có thể xử lý các dữ liệu hình ảnh nhanh hơn các máy in thông thường bằng cách nén dữ liệu in vào một kích thước nhỏ hơn để có thể nhanh chóng truyền dữ liệu đó từ máy tính sang máy in có CAPT. Tính năng này giúp tăng tốc độ in mà không phải nâng cấp bộ nhớ máy in.</p>


      <h3 class="product-info-feature">Dễ phối trang phục</h3>

      <p class="product-info-deatail">Với chiếc áo thun nam dài tay TITISHOP với màu sắc trung tính nên không hề kén đồ mà rất dễ phối với nhiều trang phục khác nhau. Bạn có thể kết hợp chiếc áo này với quần jeans, quần short và một đôi giày thể thao là đã hoàn toàn tự tin với phong cách thể thao năng động và mạnh mẽ.</p>
      


      <h3 class="product-info-feature">Đặc điểm chính:</h3>

      <p class="product-info-deatail">
      Mẫu mã : TiTi Shop (Tp.HCM)-AT82M <br>
      Kích thước sản phẩm (D x R x C cm) : 20X5X10 <br>
      Trọng lượng (KG) : 0.2 <br>
      Sản xuất tại :     Việt Nam <br>
      Loại hình bảo hành :     Không hỗ trợ bảo hành</p>


</div>
