
<div class="product-info col-xs-12">
      <h3 class="product-info-title">Giới thiệu về sản phẩm</h3>
      
      <h3 class="product-info-feature">Chất lượng cao</h3>
      <p class="product-info-deatail"> Bóp ví nam da bò thời trang, được làm từ da bò bền bỉ, chắc chắn, từng chi tiết đều được trau chuốt tỉ mỉ, đep mắt.</p>

      <p><img title="Ví da thời trang tiện lợi" alt="Ví da" src="<?php echo base_url()?>public/images/vida2.jpg" class="product-info-pic img-responsive"></p>

      <h3 class="product-info-feature">Tiện Lợi</h3>

      <p class="product-info-deatail"> Ví có kiểu dáng ngang đơn giản nhưng tiện lợi trong việc lưu giữ tiền bạc, giấy tờ xe, thẻ ATM, hình ảnh lưu niệm. đặc biệt bên trong ngăn phụ của ví còn có dây kéo, để bạn lưu giữ tiền xu, thẻ nhớ.</p>

      <p><img title="Ví da" alt="Ví da" src="<?php echo base_url()?>public/images/vida4.jpg" class="product-info-pic img-responsive"></p>

      <h3 class="product-info-feature">Lịch Lãm</h3>

      <p class="product-info-deatail">Bóp ví nam có tổng thể trơn bóng, điểm nhấn là đường viền chỉ may xéo ở góc ví tạo sự riêng biệt nổi bật.</p>
      
      <p><img title="Ví da" alt="Ví da" src="<?php echo base_url()?>public/images/vida3.jpg" class="product-info-pic img-responsive"></p>

      <h3 class="product-info-feature">Đa dạng màu sắc</h3>

      <p class="product-info-deatail">Ngoài màu nâu lịch lãm, ví nam da bò còn có màu đen sang trọng, để bạn dễ dàng lựa chọn.</p>


</div>
