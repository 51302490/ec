
<div class="col-xs-12">
    <h3 class="product-info-title">Hỏi đáp về sản phẩm</h3>
    <div class="well">
        <h4 class="product-cmt-question">Bạn có thắc mắc?</h4>
        <div class="product-cmt-addcomt clearfix">
            <input type="text" class="col-sm-10 col-xs-12" id="userComment" placeholder="Hãy đặt câu hỏi liên quan đến sản phẩm..." />
            <div class="col-sm-2 col-xs-12" style="padding: 0">
                <button class="btn btn-primary btn-auction" id="btn-addcomt"><span class="glyphicon glyphicon-comment"></span> Add Comment</button>
            </div>
        </div>

        <hr data-brackets-id="12673">
        <div data-brackets-id="12674" id="sortable" class="list-unstyled ui-sortable">
            <div class="myrow clearfix">
                <div class="product-cmt-ava col-sm-1 col-xs-2">
                    <img src="<?php echo base_url()?>public/images/member-ngan.jpg" alt="Ngân Kim" title="Ngân Kim">
                </div>
                <div class="product-cmt-content col-sm-11 col-xs-10">
                    <strong class="pull-left primary-font product-cmt-name">Ngân Kim </strong>
                    <small class="pull-right text-muted product-cmt-time">
                        <span class="glyphicon glyphicon-time"></span> &nbsp; 7 mins ago
                    </small>
                    <p class="user-comt-content">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
                </div>
            </div>
            
            <div class="myrow clearfix">
                 <div class="product-cmt-ava col-sm-1 col-xs-2">
                    <img src="<?php echo base_url()?>public/images/member-danh.jpg" alt="Trọng Trí" title="Trọng Trí">
                </div>
                <div class="product-cmt-content col-sm-11 col-xs-10">
                    <strong class="pull-left primary-font product-cmt-name">Trọng Trí </strong>
                    <small class="pull-right text-muted product-cmt-time">
                        <span class="glyphicon glyphicon-time"></span> &nbsp; 14 mins ago
                    </small>
                    <p class="user-comt-content">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                </div>
            </div>
            
           <div class="myrow clearfix">
               <div class="product-cmt-ava col-sm-1 col-xs-2">
                <img src="<?php echo base_url()?>public/images/member-hao.jpg" alt="Viết Huy Phan" title="Viết Huy Phan">
                </div>
                <div class="product-cmt-content col-sm-11 col-xs-10">
                    <strong class="pull-left primary-font product-cmt-name">Viết Huy Phan </strong>
                    <small class="pull-right text-muted product-cmt-time">
                        <span class="glyphicon glyphicon-time"></span> &nbsp; 14 mins ago
                    </small>
                    <p class="user-comt-content">Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. </p>
                </div>     
           </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    function addComment(){
    var userComment = document.getElementById("userComment").value;
    document.getElementById("ui-state-default").innerHTML = userComment;
}
</script>