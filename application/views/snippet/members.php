<div class="member-contain col-sm-4 col-xs-12">
	<div class="member-info col-sm-6 col-xs-4">
		<img src="<?php echo base_url().'public/images/'. $member[3] ?>" class="img-rounded img-responsive" alt="<?php echo $member[0] ?>" title="<?php echo $member[0] ?>">
	</div>
	<div class="member-info col-sm-6 col-xs-8">
		<h3 class="member-name"><?php echo $member[0] ?></h3>
		<p class="member-position"><?php echo $member[1] ?></p>
		<p class="member-quote"><?php echo $member[2] ?></p>
	</div>
</div>

