<div class="container">
  <div class="row">
    <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
      <form role="form" data-toggle="validator" id="my-form">
        <h2 class="tittle-add">SẢN PHẨM MỚI <small>Đấu giá nhanh và tiện lợi</small></h2>
        <hr class="colorgraph">
        <div class="form-group">
            <label class="control-label label-add" for="name">Sản Phẩm</label>
            <input type="text" name="name" id="name" class="form-control input-lg " placeholder="Tên Sản Phẩm" tabindex="1" required="true">
        </div>  
     <!--  <div class="form-group">
        <label class="control-label label-add" for="email">Email</label>
        <input type="email" name="email" id="email" class="form-control input-lg" placeholder="Email" tabindex="4" required="">
      </div> -->
        <div class="row">
          <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
              <label class="control-label label-add" for="start">Ngày Bắt Đầu</label>
              <input type="datetime-local" name="start" id="start" class="form-control input-lg" placeholder="Ngày Bắt Đầu" tabindex="5" required="">
            </div>
          </div>
          <div class="col-xs-6 col-sm-6 col-md-6">
            <div class="form-group">
            <label class="control-label label-add" for="end">Ngày Kết Thúc</label>
              <input type="datetime-local" name="end" id="end" class="form-control input-lg" placeholder="Ngày Kết Thúc" tabindex="6" required="">
            </div>
          </div>
        </div>
        <div class="form-group">
          <label class="control-label label-add" for="status">Tình Trạng</label>
          <select id="status" name="status" class="form-control input-lg">
            <option value="Tinh Trang">Tình Trạng</option>
            <option value="New">New</option>
            <option value="99%">99%</option>
            <option value="98%">98%</option>
            <option value="97%">97%</option>
            <option value="96%">96%</option>
            <option value="95%">95%</option>
            <option value="Old">Old</option>
            <option value="Second Hand">Second Hand</option>
          </select>
        </div>
        <div class="form-group">
          <label class="control-label label-add" for="tyoe">Loại Sản Phẩm</label>
      
          <select id="type" name="type" class="form-control input-lg">
            <option value="1">Công Nghệ</option>
            <option value="2">Gia Dụng</option>
            <option value="3">Thời Trang</option>
            <option value="4">Khác</option>
          </select>
        </div>
        <div class="form-group">
          <label class="control-label label-add" for="cost">Giá Khởi Điểm</label>
            <input type="text" name="cost" id="cost" class="form-control input-lg" placeholder="Giá Khởi Điểm" tabindex="4" pattern="[0-9]*" required="">
        </div>
        <div class="form-group">
          <label class="control-label label-add" for="descript">Mô Tả</label>
          <textarea name="descript" id="descript" class="form-control input-lg" placeholder="Mô tả" tabindex="4" required=""></textarea>
        </div>
        <div class="form-group">
          <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
              <label class="control-label label-add" for="picture">Hình Đại Diện</label>
              <input type="file" name="picture" id="picture" class="form-control input-lg" accept="image/*" onchange="readURL(this);" >
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6"> 
              <img alt="User Pic" src="" class=" img-responsive" id="pic-product" >
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12 col-sm-8 col-md-6 col-sm-offset-2 col-md-offset-3">
          <div class="row">
            <div class="col-xs-12">
              <input type="button" value="Xác Nhận" class="btn btn-primary btn-block btn-auction" tabindex="7" id="confirm" onclick="Addproduct(this.form)">
            </div>
            <div class="col-xs-12">
              <a href="#" data-toggle="modal" data-target="#ContactModal">Liên Hệ Với Chúng Tôi Để Quảng Cáo Tốt Hơn</a>
            </div>
          </div>
        </div>
      </div>
      <br>
      <br>      
    </form>
  </div>
</div>


</div>
<script type="text/javascript">
  $("#pic-product").hide();
  function readURL(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();
      reader.onload = function (e) {
        $('#pic-product')
          .attr('src', e.target.result)
        };
      reader.readAsDataURL(input.files[0]);
      $("#pic-product").show();
    }
  }
  $("#pic").change(function(){
    readURL(this);
  });
  var he = $("#pic-product").width();
  $("#pic-product").css("height",100);

  document.getElementById('confirm2').onclick=function(){
  var answer= confirm('Xác Nhận Đấu Giá Sản Phẩm ?');
  if(answer){

    var answer2= confirm('Bạn Có Muốn Đấu Giá Tiếp Tục? ');
    if(answer2){
      document.getElementById('my-form').reset();
    }
    else{
      window.location.href = "/index-after-login";
    }

  }

};

 function Addproduct(form){ 
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>add-product",
      data:{
        "starttime" : form.start.value,
        "endtime" : form.end.value,
        "product_condition" : form.status.value,
        "startprice" : form.cost.value,
        "description" : form.descript.value,
          "name" : form.name.value,
          "type" : form.type.value
      },
      success: function(data){
          alert("thanh cong");
      },
    });     
  };

</script>