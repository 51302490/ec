<div class="container">
	<p class="ratio-auction-para"><strong>Đấu giá thành công:</strong> <span class="ratio-auction"><?php echo $win_count.'/'.($done_count + $doing_count) ?></span></p>
  	<div class="form-group">
  		<select class="form-control" onchange="auction_filter(this.value)">
		    <option>Tất cả</option>
		    <option>Phiên đấu giá đang tham gia</option>
		    <option>Phiên đấu giá đã tham gia</option>
		    <option>Phiên khởi tạo</option>
  		</select>
	</div>

	<table class="table table-hover table-responsive" id="mytable">
		<tr>
			<th>ID</th>
			<th>Hình ảnh</th>
			<th>Người thắng</th>
			<th>Tên sản phẩm</th>
			<th>Giá cuối cùng</th>
		</tr>
		<?php for($i = 0; $i < $done_count; $i++) {
			$result = $done[$i]; ?>
			<tr class="auction-history-done">
				<td> <?php echo $result['ID'] ?></td>
				<td>
					<a href="<?php echo base_url().'product-end/'. $result['ID']?>"><img src="<?php echo base_url().'public/images/'.$result['avatar'] ?>" class="table-pic" alt="<?php echo $result['name'] ?>" title="<?php echo $result['name'] ?>"></a>					
				</td>
				<td class="winner"> <?php echo $result['username']?></td>
				<td> <a href="<?php echo base_url().'product-end/'. $result['ID']?>"><?php echo $result['name'] ?> </a></td>
				<td> <?php echo $result['currentbid'] ?></td>
			</tr>
		<?php } ?>

		<?php for($i = 0; $i < $doing_count; $i++) {
			$result = $doing[$i]; ?>
			<tr class="auction-history-doing">
				<td> <?php echo $result['ID'] ?></td>
				<td>
					<a href="<?php echo base_url().'product/'. $result['ID']?>"><img src="<?php echo base_url().'public/images/'.$result['avatar'] ?>" class="table-pic" alt="<?php echo $result['name'] ?>" title="<?php echo $result['name'] ?>"></a>					
				</td>
				<td class="winner"></td>
				<td><a href="<?php echo base_url().'product/'. $result['ID']?>"> <?php echo $result['name'] ?> </a></td>
				<td> <?php echo $result['currentbid'] ?></td>
			</tr>
		<?php } ?>

		<?php for($i = 0; $i < $mysell_count; $i++) {
				$result = $mysell[$i]; 
				$current = date('Y-m-d H:i:s');
				if ($result['endtime'] > $current) $isEnd = false;
				else $isEnd = true;
		 ?>
			<tr class="auction-history-me">
				<td> <?php echo $result['ID'] ?></td>
				<td>
					<?php if (!$isEnd) { ?>
						<a href="<?php echo base_url().'product/'. $result['ID']?>"><img src="<?php echo base_url().'public/images/'.$result['avatar'] ?>" class="table-pic" alt="<?php echo $result['name'] ?>" title="<?php echo $result['name'] ?>"></a>
					<?php } else { ?>
						<a href="<?php echo base_url().'product-end/'. $result['ID']?>"><img src="<?php echo base_url().'public/images/'.$result['avatar'] ?>" class="table-pic" alt="<?php echo $result['name'] ?>" title="<?php echo $result['name'] ?>"></a>	
					<?php } ?>					
				</td>
				<td class="winner"> 
				<?php 
					if (!$isEnd) echo "";
					else echo $result['username']; ?>
				</td>
				<td> 
					<?php if (!$isEnd) { ?>
						<a href="<?php echo base_url().'product/'. $result['ID']?>"> <?php echo	$result['name'] ?> </a>
					<?php } else { ?>
						<a href="<?php echo base_url().'product-end/'. $result['ID']?>"> <?php echo	$result['name'] ?> </a>
					<?php } ?> 
				</td>
				<td> <?php echo $result['currentbid'] ?></td>
			</tr>
		<?php } ?>
	</table>
</div>

<script type="text/javascript">
	function auction_filter(value){
		if (value == "Tất cả"){
			$(".auction-history-me").show();
			$(".auction-history-done").show();
			$(".auction-history-doing").show();
		}
		else if (value == "Phiên đấu giá đang tham gia"){
			$(".auction-history-me").hide();
			$(".auction-history-done").hide();
			$(".auction-history-doing").show();
		}
		else if (value == "Phiên đấu giá đã tham gia"){
			$(".auction-history-me").hide();
			$(".auction-history-done").show();
			$(".auction-history-doing").hide();
		}
		// Phiên khởi tạo
		else {
			$(".auction-history-me").show();
			$(".auction-history-done").hide();
			$(".auction-history-doing").hide();
		}
	}
</script>

