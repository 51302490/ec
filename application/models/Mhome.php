<?php
class Mhome extends CI_Model{
	protected $table = 'products';
	
	public function getProducts(){
		$this->db->where("endtime > ", date('Y-m-d H:i:s'));
		return $this->db->get($this->table)->result_array();
	} 

	public function get_row_count($myarray){
		return count($myarray);
	}

	public function getType($type){
		$this->db->where("endtime > ", date('Y-m-d H:i:s'));
		$this->db->where('type', $type);
		return $this->db->get($this->table)->result_array();
	}

	public function get_recent_end(){
		$current = date('Y-m-d H:i:s');
		$this->db->select('products.ID, products.avatar, name, username, currentbid');
		$this->db->from($this->table);
		$this->db->join('members',"members.ID = products.winner AND products.endtime < '$current' ");
		$this->db->order_by('endtime','desc');
		$query = $this->db->get();
		return $query->result_array();
	}

	public function get_endtype($type){
		$current = date('Y-m-d H:i:s');
		$this->db->select('products.ID, products.avatar, name, username, currentbid');
		$this->db->from($this->table);
		$this->db->join('members',"members.ID = products.winner AND products.type = '$type' AND products.endtime < '$current' ");
		$this->db->order_by('endtime','desc');

		$query = $this->db->get();
		return $query->result_array();
	}

	public function showprice($price){
		$len = strlen($price);
		if ($len % 3 == 0)
			$loop = $len/3 - 1;
		else
			$loop = $len/3 ;
		for ($i = 1; $i <= $loop; $i++){
			$price = substr_replace($price,' ',$len-$i*3, 0);
		}
		return $price;
	}

	public function getResult(){
		$current = date('Y-m-d H:i:s');
		$this->db->select('products.ID, name, products.avatar, product_condition, starttime, endtime, currentbid, startprice, auctions, description, username AS winner');
		$this->db->from($this->table);
		$this->db->join('members', "members.ID = products.winner AND products.endtime < '$current'");
		$data = $this->db->get();
		return $data->result_array();
	} 

	// NOT USED
	public function getWin(){
		$current = date('Y-m-d H:i:s');
		$id = $this->session->userdata('userID');

		$this->db->select('products.ID, members.username, products.avatar, products.name, products.currentbid');
		$this->db->from($this->table);
		$this->db->join('members', "products.winner = members.ID AND products.winner = '$id' AND products.endtime < '$current'");
		$win = $this->db->get();
		return $win->result_array();
	}
	public function getFail(){
		$current = date('Y-m-d H:i:s');
		$id = $this->session->userdata('userID');

		$this->db->select('products.ID, members.username, products.avatar, products.name, products.currentbid');
		$this->db->from($this->table);
		$this->db->join('personal_auction as P', "P.IDproduct = products.ID AND P.IDmember = '$id' AND P.IDmember != products.winner AND products.endtime < '$current' ");

		$this->db->join('members', "products.winner = members.ID");

		$fail = $this->db->get();
		return $fail->result_array();
	}
	// END NOT USED

	public function getDone(){
		$current = date('Y-m-d H:i:s');
		$id = $this->session->userdata('userID');

		$this->db->select('products.ID, members.username, products.avatar, products.name, products.currentbid');
		$this->db->from($this->table);
		$this->db->join('personal_auction as P', "P.IDproduct = products.ID AND P.IDmember = '$id' AND products.endtime < '$current' ");

		$this->db->join('members', "products.winner = members.ID");

		$done = $this->db->get();
		return $done->result_array();
	}
	public function getDoing(){
		$current = date('Y-m-d H:i:s');
		$id = $this->session->userdata('userID');

		$this->db->select('products.ID, products.avatar, products.name, products.currentbid');
		$this->db->from($this->table);
		$this->db->join('personal_auction as P', "P.IDproduct = products.ID AND P.IDmember = '$id' AND products.endtime >'$current' ");

		$doing = $this->db->get();
		return $doing->result_array();
	}
	public function getMySell(){
		$id = $this->session->userdata('userID');

		$this->db->select('products.ID, endtime, members.username, products.avatar, products.name, products.currentbid');
		$this->db->from('products');
		$this->db->join('members', "products.winner = members.ID AND products.seller = '$id'");
		$mysell = $this->db->get();
		return $mysell->result_array();
	}
	public function getSearchList($key){
		$this->db->select('products.ID,products.avatar,products.name');
		$this->db->from($this->table);
		$this->db->like('products.name',$key);
		$data=$this->db->get();
		return $data->result_array();
	}
	public function get_autocomplete($search_data) {
        $this->db->select('products.name,products.avatar,products.endtime');
        $this->db->select('products.ID');
        $this->db->like('products.name', $search_data);
        return $this->db->get('products', 10);
    }
}