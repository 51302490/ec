<?php
class Muser extends CI_Model{
	protected $table = 'members';
	
	public function login($email, $password){
		$this->db->where('email', $email);
		$this->db->where('password', md5($password));
		$data = $this->db->get($this->table);

		if ($data->num_rows() > 0){
			$user = $this->getAvatar($email);
			$this->session->set_userdata('userID', $user[0]['ID']);
			$this->session->set_userdata('userName', $user[0]['username']);
			$this->session->set_userdata('userAvatar', $user[0]['avatar']);
		}
		
		return $data->num_rows();
	} 
	public function getAvatar ($email){
		$this->db->select('username, avatar, ID');
		$this->db->where('email', $email);
		return $this->db->get($this->table)->result_array();
	}
	public function signup ($email, $username, $pass, $repass, $birthday){
		
		$insert = array(
			'email' => $email,
			'username' => $username,
			'password' => md5($pass),
			'createday' => date('Y-	n-j'),
			'birthday' => $birthday
		);
		$data = $this->db->insert($this->table, $insert);
		if ($data > 0) {
			$this->login($email, $pass);
		}
	}
	public function getInfo($id) {
		$this->db->select('*');
		$this->db->where('ID', $id);
		return $this->db->get($this->table)->result_array();
	}
	public function getUpdate($update){
		$id = $this->session->userdata('userID');
		$this->db->where('id', $id);
		$this->db->where('password', md5($update['password']));
		$affected = $this->db->get($this->table);

		if ($affected->num_rows() > 0){
			$this->db->set('userName', $update['username']);
			if ($update['repassword'] != "")
				$this->db->set('password', md5($update['repassword']));
			$this->db->set('birthday', $update['birthday']);
			$this->db->set('avatar', $update['avatar']);
			$this->db->where('id', $id);
			$this->db->update($this->table);
		}
		return $affected->num_rows();
	}
}