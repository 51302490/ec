<?php
class MProduct extends CI_Model{
	protected $table = 'products';
	
	public function getDescription($id = 0){
		$this->db->select('products.ID, name, product_condition, starttime, endtime, currentbid, startprice, auctions, description, username');
		$this->db->from($this->table);
		$this->db->join('members', "members.ID = products.seller AND products.ID = '$id'");
		$data = $this->db->get();
		return $data->result_array();
	} 

	public function getRecentEnd($id){
		$this->db->select('products.ID, name, product_condition, S.username AS seller, W.username AS winner, endtime, startprice, auctions, description, currentbid');
		$this->db->from($this->table);
		$this->db->join('members AS S', "S.ID = products.seller AND products.ID = '$id'");
		$this->db->join('members AS W', 'products.winner = W.ID');
		$data = $this->db->get();
		return $data->result_array();
	}

	public function showprice($price){
		$len = strlen($price);
		if ($len % 3 == 0)
			$loop = $len/3 - 1;
		else
			$loop = $len/3 ;
		for ($i = 1; $i <= $loop; $i++){
			$price = substr_replace($price,' ',$len-$i*3, 0);
		}
		return $price;
	}

	public function getNewAuction($id,$bid){
		$this->db->select('currentbid, auctions');
		$this->db->where('ID',$id);
		$result = $this->db->get($this->table)->result_array();
		
		if ($result[0]['currentbid'] >= $bid)
			return 0;
		else{
			$auctions = $result[0]['auctions'] + 1;
			$userID = $this->session->userdata['userID'];
	
			$update = array(
					'currentbid' => $bid,
					'winner' => $userID,
					'auctions' => $auctions
				);
			$this->db->set($update);
			$this->db->where('ID', $id);
			$this->db->update($this->table);

			// Update Table PERSONAL AUCTION
			$this->db->select('*');
			$this->db->where('IDproduct', $id);
			$this->db->where('IDmember', $userID);
			$personal = $this->db->get('personal_auction');
			if ($personal->num_rows() > 0){
				$count = $personal->result_array()[0]['count'] + 1;
				$this->db->set('highest_price', $bid);
				$this->db->set('count', $count);
				$this->db->where('IDproduct', $id);
				$this->db->where('IDmember', $userID);
				$this->db->update('personal_auction');
			}
			else {
				$insert = array(
					'IDproduct' => $id,
					'IDmember' => $userID,
					'highest_price' => $bid,
					'count' => '1'
				);
				$this->db->set($insert);
				$this->db->insert('personal_auction');
			}
			return 1;
		}
	}
	public function add($product){
			$this->db->set($product);
			$this->db->insert('products');
			return 1;
	}
}