<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	protected $data;
	function __construct() {
        // Gọi đến hàm khởi tạo của cha
        parent::__construct();
        $this->load->helper('url');
    	$this->load->library('session');
      	$this->load->model('muser');
      	date_default_timezone_set('Asia/Ho_Chi_Minh');
    }
	public function login() {
		$email = $this->input->post('email');
		$pass = $this->input->post('password');
		if ($this->muser->login($email, $pass) > 0){
			echo 'success';	
			echo $this->session->userdata('userName');
		}
		else{
			echo 'wrong';
		}
	}
	public function logout() {
		$this->session->sess_destroy();
		header('Location:http://localhost/EC/');
	}
	public function signup(){
		$email = $this->input->post('email');
		$username = $this->input->post('username');
		$pass = $this->input->post('password');
		$repass = $this->input->post('repassword');
		$birthday = $this->input->post('birthday');
		if ($this->muser->signup($email, $username, $pass, $repass, $birthday) > 0){
			echo 'success';
		}
	}
	public function info($id){
		$this->data['members'] = $this->muser->getInfo($id);
		$this->data['contentPage'] = 'member-page';
		$this->data['title'] = "Personal Infomation | ".$this->session->userdata('userName');
		$this->data['keywords'] = "";
		$this->data['description'] = "";
		$this->load->view('_layout', $this->data);
	}
	public function update(){
		$update = array(
			'username' => $this->input->post('username'),
			'password' => $this->input->post('password'),
			'repassword' => $this->input->post('repassword'),
			'birthday' => $this->input->post('birthday'),
			'avatar' => basename($this->input->post('image'))
			);
		$isUpdate = $this->muser->getUpdate($update);
		echo $isUpdate;
	}
}
