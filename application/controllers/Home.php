<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	protected $data;
	function __construct() {
        // Gọi đến hàm khởi tạo của cha
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('mhome');
        $this->load->library('session');
        date_default_timezone_set('Asia/Ho_Chi_Minh');
    }
    protected function slave(&$count, &$list){
		$count = $this->mhome->get_row_count($list);
		for ($i = 0; $i < $count; $i++){
			$list[$i]['currentbid'] = $this->mhome->showprice($list[$i]['currentbid']);
		}
    }
	public function index() {
		$this->data['product_list'] = $this->mhome->getProducts();
		$this->slave($this->data['product_count'], $this->data['product_list']);		
		$this->data['contentPage'] = 'index';
		$this->data['title'] = "Technology, Household, Fashion and More | Auction Online";
		$this->data['keywords'] = "Web đấu giá online uy tín nhất";
		$this->data['description'] = "Auction Online là trang web uy tín hàng đầu của Việt Nam về mảng đấu giá. Chúng tôi chuyên cung cấp các sản phẩm đa dạng và chất lượng về công nghệ, thời trang, và đồ gia dụng. Trải qua 10 kinh nghiệm, chúng tôi vẫn luôn trung thành với tiêu chí 'Đơn giản, thân thiện, trung thực'. ";
		$this->load->view('_layout', $this->data);
	}
	public function type ($type){
		$this->data['product_list'] = $this->mhome->getType($type);
		$this->slave($this->data['product_count'], $this->data['product_list']);
		$this->data['contentPage'] = 'index';
		switch ($type) {
			case 1: $title = "Technology"; 
					$ty = "công nghệ";
					break;
			case 2: $title = "Household"; 
					$ty = "gia dụng";
					break;
			case 3: $title = "Fashion";
					$ty = "thời trang, may mặc";
					break;
			default: $title = "Others"; 
					$ty = "sách, truyện tranh";
					break;
		}
		$this->data['title'] = $title." | Auction Online";
		$this->data['keywords'] = "Bán đấu giá các sản phẩm"." ".$ty;
		$this->data['description'] = "Trang web đấu giá online uy tín nhất tại Việt Nam. Với hơn 5000 người dùng và hơn 15000 sản phẩm được cập nhật thường xuyên";
		$this->load->view('_layout', $this->data);
	}
	public function recent_end(){
		$this->data['product_list'] = $this->mhome->get_recent_end();
		$this->slave($this->data['product_count'], $this->data['product_list']);	
		$this->data['contentPage'] = 'auction-end-page';
		$this->data['title'] = "Technology, Household, Fashion and More | Auction End";
		$this->data['keywords'] = "Đấu giá các sản phẩm công nghệ, thời trang, gia dụng";
		$this->data['description'] = "Auction Online đã trải qua hơn 10 năm kinh nghiệm và có hơn 10000 sản phẩm đã đấu giá và kết thúc thành công.";
		$this->load->view('_layout', $this->data);
	}
	public function endtype($type){
		$this->data['product_list'] = $this->mhome->get_endtype($type);
		$this->slave($this->data['product_count'], $this->data['product_list']);
		$this->data['contentPage'] = 'auction-end-page';
		switch ($type) {
			case 1: $title = "Technology"; break;
			case 2: $title = "Household"; break;
			case 3: $title = "Fashion"; break;
			default: $title = "Others"; break;
		}
		$this->data['title'] = $title." | Auction End";
		$this->data['keywords'] = "Đấu giá uy tín ở Việt Nam";
		$this->data['description'] = "Auction Online chuyên đấu giá các sản phẩm công nghệ, gia dụng, thời trang và sách truyện";
		$this->load->view('_layout', $this->data);
	}
	public function auction_result(){
		$this->data['product_list'] = $this->mhome->getResult();
		$this->slave($this->data['product_count'], $this->data['product_list']);
		$this->data['contentPage'] = 'auction-result-page';
		$this->data['title'] = "Technology, Household, Fashion and More | Auction Table Result";
		$this->data['keywords'] = "Đấu giá uy tín";
		$this->data['description'] = "Bảng tóm tắt thông tin các sản phẩm đã đấu giá thành công trong thời gian gần đây.";
		$this->load->view('_layout', $this->data);
	}
	public function auction_history(){
		$this->data['done'] = $this->mhome->getDone();
		$this->slave($this->data['done_count'], $this->data['done']);

		$this->data['doing'] = $this->mhome->getDoing();
		$this->slave($this->data['doing_count'], $this->data['doing']);

		$this->data['mysell'] = $this->mhome->getMySell();
		$this->slave($this->data['mysell_count'], $this->data['mysell']);
		
		$this->data['win'] = $this->mhome->getWin();
		$this->slave($this->data['win_count'], $this->data['win']);

		$this->data['contentPage'] = 'auction-history-page';
		$this->data['title'] = "Auction History | ".$this->session->userdata('userName');
		$this->data['keywords'] = "Thông tin lịch sử đấu giá";
		$this->data['description'] = "Bảng tóm tắt lịch sử đấu giá của ".$this->session->userdata('userName');
		$this->load->view('_layout', $this->data);
	}
	public function search(){
		$list=$this->data['product_list'] = $this->mhome->getSearchList($_POST['search_data']);
		$this->data['product_count'] = $this->mhome->get_row_count($list);
		$this->data['contentPage'] = 'search-product-page';
		$this->data['title']="Searching result";
		$this->data['keywords'] = "Tìm kiếm sản phẩm đấu giá";
		$this->data['description'] = "Sản phẩm được tìm kiếm ";
		$this->load->view('_layout',$this->data);
	}
    public function autocomplete() {
        $search_data = $_POST['search_data'];
        $query = $this->mhome->get_autocomplete($search_data);
        $current = date('Y-m-d H:i:s');
        foreach ($query->result() as $row):
        	if(($row->endtime) > $current ){
            echo "<li><a href='" . base_url() . "product/" . $row->ID . "'><img id='searchimg' src='" . base_url() . "public/images/" . $row->avatar . "'>". $row->name . "</a></li>";
        	}
        	else{
        		 echo "<li><a href='" . base_url() . "product-end/" . $row->ID . "'><img id='searchimg' src='" . base_url() . "public/images/" . $row->avatar . "'>". $row->name . "</a></li>";
        	}
        endforeach;
    }
}
