<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class About_us extends CI_Controller {
	protected $data;
	function __construct() {
        // Gọi đến hàm khởi tạo của cha
        parent::__construct();
        $this->load->library('session');
    }
	public function index() {
		$this->data['contentPage'] = 'about-us-page';
		$this->data['title'] = "Auction Online | About Us";
		$this->data['keywords'] = "About Us Auction Online";
		$this->data['description'] = "Auction Online là trang web uy tín hàng đầu của Việt Nam về mảng đấu giá. Chúng tôi chuyên cung cấp các sản phẩm đa dạng và chất lượng về công nghệ, thời trang, và đồ gia dụng. Trải qua 10 kinh nghiệm, chúng tôi vẫn luôn trung thành với tiêu chí 'Đơn giản, thân thiện, trung thực'.";
		$this->load->view('_layout', $this->data);
	}
}
