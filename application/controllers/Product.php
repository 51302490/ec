<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Product extends CI_Controller {
	protected $data;
	function __construct() {
        // Gọi đến hàm khởi tạo của cha
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('mproduct');
        $this->load->library('session');
    }
    protected function slave(){
    	$this->data['product'][0]['startprice'] = $this->mproduct->showprice($this->data['product'][0]['startprice']);
		$this->data['product'][0]['currentbid'] = $this->mproduct->showprice($this->data['product'][0]['currentbid']);
    }
	public function index($id=0) {
		$this->data['product'] = $this->mproduct->getDescription($id);
		$this->slave();
		$this->data['contentPage'] = 'product-description-page';
		$this->data['title'] = $this->data['product'][0]['name']." | Auction Online";
		$this->data['keywords'] = "Đấu giá ".$this->data['product'][0]['name'];
		$this->data['description'] = "Thông tin đấu giá của ".$this->data['product'][0]['name'];
		$this->load->view('_layout', $this->data);
	}
	public function recent_end($id){
		$this->data['product'] = $this->mproduct->getRecentEnd($id);
		$this->slave();
		$this->data['contentPage'] = 'product-end-description-page';
		$this->data['title'] = $this->data['product'][0]['name']." | Auction End";
		$this->data['keywords'] = "Đấu giá ".$this->data['product'][0]['name'];
		$this->data['description'] = "Thông tin đấu giá của ".$this->data['product'][0]['name'];
		$this->load->view('_layout', $this->data);
	}
	public function add_product(){
				$auction=0;
			$product = array(
			'name' => $this->input->post('name'),
			'type' => $this->input->post('type'),
			'product_condition' => $this->input->post('product_condition'),
			'seller' => '3',
			'starttime' => $this->input->post('starttime'),
			'endtime' => $this->input->post('endtime'),
			'currentbid' => $this->input->post('startprice'),
			'winner' => '3',
			'startprice' => $this->input->post('startprice'),
			'auctions' => '0',
			'avatar' => 'laptop.jpg',
			'images' => 'laptop.jpg',
			'description' => $this->input->post('description')
			
			);
		$this->mproduct->add($product);
		$this->data['contentPage'] = "add-product-page";
		$this->data['title'] = "Add New Auction | ".$this->session->userdata('userName');
		$this->data['keywords'] = "";
		$this->data['description'] = "";
		$this->load->view('_layout', $this->data);
	}
	public function new_auction($id){
		$bid = $_GET['bid'];
		$isUpdate = $this->mproduct->getNewAuction($id, $bid);
		echo $isUpdate;
	}
}
