<?php
  
   class Upload extends CI_Controller {
	    protected $data; 
      public function __construct() {
       
         parent::__construct(); 
         $this->load->helper(array('form', 'url')); 
         $this->load->model('muser');
         $this->load->library('session');
      }
		
      public function index() { 
         $this->load->view('upload_form', array('error' => ' ' )); 
      } 
		
      public function do_upload($id) { 
         $config['upload_path']   = './public/images/'; 
         $config['allowed_types'] = 'gif|jpg|png'; 
         $config['max_size']      = 1111; 
         $config['max_width']     = 101124; 
         $config['max_height']    = 761118;  
         $this->load->library('upload', $config);
			$this->upload->do_upload('userfile');
      $this->data['members'] = $this->muser->getInfo($id);
      $this->data['contentPage'] = 'member-page';
      $this->data['title'] = "Personal Infomation | ".$this->session->userdata('userName');
      $this->data['keywords'] = "";
      $this->data['description'] = "";
      $this->load->view('_layout', $this->data);

      } 
   } 
?>