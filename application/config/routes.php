<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

// Home
$route['default_controller'] = "Home";
$route['home/technology'] = "Home/type/1";
$route['home/household'] = "Home/type/2";
$route['home/fashion'] = "Home/type/3";
$route['home/others'] = "Home/type/4";
$route['recently-end'] = "Home/recent_end";
$route['recently-end/technology'] = "Home/endtype/1";
$route['recently-end/household'] = "Home/endtype/2";
$route['recently-end/fashion'] = "Home/endtype/3";
$route['recently-end/others'] = "Home/endtype/4";
$route['auction-result'] = "Home/auction_result";
$route['auction-history'] = "Home/auction_history";
$route['search']="Home/search";
$route['autosearch'] = "Home/autocomplete";
// Product
$route['product/(:any)'] = "Product/index/$1";
$route['product-end/(:any)'] = "Product/recent_end/$1";
$route['add-product'] = "Product/add_product";
$route['new-auction/(:any)'] = "Product/new_auction/$1";
// User
$route['login'] = "User/login";
$route['logout'] = "User/logout";
$route['signup'] = "User/signup";
$route['member-info/(:any)'] = "User/info/$1";
$route['update-info'] = "User/update";
$route['upload'] = 'Upload';
$route['upload/do_upload/(:any)'] = 'Upload/do_upload/$1'; 
// About-us
$route['about-us'] = "About_us";

$route['404_override'] = '';

/* End of file routes.php */
/* Location: ./application/config/routes.php */