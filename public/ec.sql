-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 22, 2016 at 05:35 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 7.0.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ec`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `ID` int(11) NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `createday` date NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT 'default-avatar.png',
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(20) COLLATE utf8_unicode_ci DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `money` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`ID`, `email`, `username`, `password`, `createday`, `avatar`, `address`, `phone`, `birthday`, `money`) VALUES
(1, '51301515@hcmut.edu.vn', 'Viết Huy Phan', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-13', 'member-hao.jpg', '', NULL, NULL, 500000),
(2, '51304369@hcmut.edu.vn', 'Trọng Trí', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-13', 'member-danh.jpg', '', NULL, NULL, 250000),
(3, 'kimngan.bk13@gmail.com', 'Ngân Kim', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-13', 'member-ngan.jpg', '175/25 Nguyễn Chế Nghĩa P.12 Q.8 TPHCM', '0906 869 871', '1995-04-17', 19500000),
(4, '51302917@hcmut.edu.vn', 'Diệp Bảo Phi', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-19', 'default-avatar.png', '', NULL, NULL, 0),
(5, 'dennisphuc@gmail.com', 'Phúc Võ', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-19', 'default-avatar.png', '', NULL, NULL, 0),
(6, '51301212@hcmut.edu.vn', 'Hiền Nguyễn', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-19', 'default-avatar.png', '', NULL, NULL, 0),
(7, '51301012@hcmut.edu.vn', 'Hà Cao Hào', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-19', 'default-avatar.png', '', NULL, NULL, 0),
(8, '51300505@hcmut.edu.vn', 'Danh Võ', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-19', 'default-avatar.png', '', NULL, NULL, 0),
(9, '51204067@hcmut.edu.vn', 'Trí Phạm', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-19', 'default-avatar.png', '', NULL, NULL, 0),
(10, 'ngan@gmail.com', 'Ahihi', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-19', 'default-avatar.png', '', NULL, '0000-00-00', 0),
(12, 'ngan2@gmail.com', 'Ahehe', 'e10adc3949ba59abbe56e057f20f883e', '2016-11-19', 'default-avatar.png', '', NULL, '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `personal_auction`
--

CREATE TABLE `personal_auction` (
  `IDproduct` int(11) NOT NULL,
  `IDmember` int(11) NOT NULL,
  `highest_price` int(11) NOT NULL,
  `count` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `personal_auction`
--

INSERT INTO `personal_auction` (`IDproduct`, `IDmember`, `highest_price`, `count`) VALUES
(1, 3, 180000, 5),
(2, 2, 70000, 1),
(3, 3, 62000, 9),
(3, 6, 45000, 1),
(4, 3, 70000, 1),
(4, 8, 60000, 1),
(5, 3, 145000, 3),
(5, 5, 100000, 1),
(6, 3, 120000, 1),
(6, 9, 100000, 1),
(7, 3, 80000, 1),
(8, 4, 60000, 1),
(9, 6, 50000, 1),
(10, 9, 40000, 1),
(11, 3, 50000, 1),
(12, 3, 350000, 1),
(13, 1, 15970000, 1),
(13, 3, 10000000, 1),
(14, 3, 25000000, 1),
(15, 1, 350000, 1),
(15, 3, 330000, 1),
(16, 6, 185000, 1),
(17, 7, 487000, 1);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `ID` int(11) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` int(11) NOT NULL,
  `product_condition` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seller` int(11) NOT NULL,
  `starttime` datetime DEFAULT NULL,
  `endtime` datetime NOT NULL,
  `currentbid` int(11) NOT NULL,
  `winner` int(255) DEFAULT NULL,
  `startprice` int(11) NOT NULL,
  `auctions` int(11) NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `images` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'product-info-canon.php'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`ID`, `name`, `type`, `product_condition`, `seller`, `starttime`, `endtime`, `currentbid`, `winner`, `startprice`, `auctions`, `avatar`, `images`, `description`) VALUES
(1, 'Canon LBP 2900', 1, 'Mới 99%', 1, '2016-11-15 09:30:00', '2016-11-30 23:30:00', 180000, 3, 50000, 14, 'canon01.jpg', 'canon01.jpg,canon02.jpg,canon03.jpg,canon04.jpg', 'product-info-canon.php'),
(2, 'Áo thun', 3, 'Mới', 2, '2016-11-16 08:00:00', '2016-11-30 22:00:00', 70000, 7, 40000, 2, 'aothun.jpg', 'aothun.jpg', 'product-info-aothun.php'),
(3, 'Ví da', 3, 'Mới 98%', 1, '2016-11-16 14:30:00', '2016-11-28 16:45:00', 62000, 3, 30000, 11, 'vida.jpg', 'vida.jpg', 'product-info-vida.php'),
(4, 'Thắt lưng nam', 3, 'Mới', 2, '2016-11-15 14:45:00', '2016-11-29 15:00:00', 70000, 3, 50000, 2, 'thatlung.jpg', 'thatlung.jpg', 'product-info-thatlung.php'),
(5, 'Giày lười nam', 3, 'Mới', 2, '2016-11-16 15:00:00', '2016-11-28 22:00:00', 145000, 3, 70000, 6, 'giayluoinam.jpg', 'giayluoinam.jpg', 'product-info-giay.php'),
(6, 'Balo', 3, 'Mới 98%', 6, '2016-11-16 11:20:00', '2016-11-28 17:30:00', 120000, 3, 50000, 4, 'Balo.jpg', 'Balo.jpg', 'product-info-canon.php'),
(7, 'Kính râm', 3, 'Mới', 1, '2016-11-16 00:00:00', '2016-11-30 22:00:00', 80000, 7, 40000, 3, 'kinhram.jpg', 'kinhram.jpg', 'product-info-canon.php'),
(8, 'Dép lào', 3, 'Mới', 2, '2016-11-17 08:30:00', '2016-11-29 14:30:00', 60000, 4, 10000, 5, 'deplao.jpg', 'deplao.jpg', 'product-info-canon.php'),
(9, 'Nón lưỡi trai', 3, 'Mới', 3, '2016-11-16 10:30:00', '2016-11-29 10:30:00', 50000, 3, 10000, 11, 'nonluoitrai.jpg', 'nonluoitrai.jpg', 'product-info-canon.php'),
(10, 'Tony buổi sáng', 4, 'Mới', 7, NULL, '2016-11-18 00:00:00', 40000, 9, 10000, 3, 'tony01.jpg', '', 'product-info-canon.php'),
(11, 'Đắc Nhân Tâm', 4, 'Mới', 6, NULL, '2016-11-18 00:00:00', 50000, 3, 0, 10, 'dacnhantam01.jpg', '', 'product-info-canon.php'),
(12, 'Bếp nướng điện', 2, 'Mới', 8, NULL, '2016-11-18 00:00:00', 350000, 3, 20000, 37, 'bepnuong01.jpg', '', 'product-info-canon.php'),
(13, 'Iphone 6', 1, 'Mới', 3, '2016-11-15 00:00:00', '2016-11-18 00:00:00', 15970000, 1, 1000000, 53, 'iphone01.jpg', '', 'product-info-canon.php'),
(14, 'Macbook', 1, 'Mới', 7, NULL, '2016-11-18 00:00:00', 25000000, 3, 1500000, 137, 'macbook01.png', '', 'product-info-canon.php'),
(15, 'Hộp cơm', 2, 'Mới', 5, NULL, '2016-11-18 00:00:00', 350000, 1, 35000, 22, 'hopcom01.jpg', '', 'product-info-canon.php'),
(16, 'Chảo chống dính', 2, 'Mới', 9, NULL, '2016-11-19 14:00:00', 185000, 6, 35000, 12, 'chaochongdinh01.png', '', 'product-info-canon.php'),
(17, 'Nồi canh', 2, 'Mới', 8, NULL, '2016-11-17 00:00:00', 487000, 7, 50000, 18, 'noi01.jpg', '', 'product-info-canon.php');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`ID`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `personal_auction`
--
ALTER TABLE `personal_auction`
  ADD PRIMARY KEY (`IDproduct`,`IDmember`),
  ADD KEY `IDmember` (`IDmember`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`ID`),
  ADD KEY `seller` (`seller`),
  ADD KEY `winner` (`winner`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `personal_auction`
--
ALTER TABLE `personal_auction`
  ADD CONSTRAINT `personal_auction_ibfk_1` FOREIGN KEY (`IDproduct`) REFERENCES `products` (`ID`),
  ADD CONSTRAINT `personal_auction_ibfk_2` FOREIGN KEY (`IDmember`) REFERENCES `members` (`ID`);

--
-- Constraints for table `products`
--
ALTER TABLE `products`
  ADD CONSTRAINT `products_ibfk_1` FOREIGN KEY (`seller`) REFERENCES `members` (`ID`),
  ADD CONSTRAINT `products_ibfk_2` FOREIGN KEY (`winner`) REFERENCES `members` (`ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
