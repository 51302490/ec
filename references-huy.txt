﻿*CODE*
query in codeigniter:
http://www.codeigniter.com/userguide3/database/query_builder.html#selecting-data

auto complete search:
http://stackoverflow.com/questions/34215301/ajax-auto-complete-search-with-code-igniter

dynamic countdown : 
https://forums.phpfreaks.com/topic/231768-dynamic-countdown/

datetime function in mysql : 
https://dev.mysql.com/doc/refman/5.5/en/date-and-time-functions.html

event schedule in mysql : 
https://dev.mysql.com/doc/refman/5.7/en/event-scheduler.html

form submit in ajax :
http://stackoverflow.com/questions/1960240/jquery-ajax-submit-form

query in mysql : 
https://www.tutorialspoint.com/mysql/mysql-select-query.htm

php countdown timer:
https://www.youtube.com/watch?v=wjUPfh6Vnbo

structure of codeigniter :
http://freetuts.net/cau-truc-folder-codeigniter-39.html

live search in php :
http://www.w3schools.com/php/php_ajax_livesearch.asp

upload file :
https://www.tutorialspoint.com/codeigniter/codeigniter_file_uploading.htm